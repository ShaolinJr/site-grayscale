
// jQuery to collapse the navbar on scroll
function collapseNavbar() {
    if ($(".navbar").offset().top > 50) {
        // $(".navbar-fixed-top").addClass("top-nav-collapse");
        // $('.navbar-custom.top-nav-collapse .navbar-brand img').attr('src','img/casa-logo-colorida.png');
        // $('.navbar-custom.top-nav-collapse .navbar-brand img').css('width','80%');
        // $('.navbar-custom.top-nav-collapse .navbar-brand img').css('height','auto');
    } else {
        // $('.navbar.navbar-custom.navbar-fixed-top .navbar-brand img').attr('src','img/esf-logo-branca.png');
        if (isMobile()) {
            $('.navbar-custom.top-nav-collapse .navbar-brand img').css('width', '100%');
            $('.navbar-custom.top-nav-collapse .navbar-brand img').css('height', 'auto');
        }
        // $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
}

// $(window).scroll(collapseNavbar);
$(document).ready(function(){
    collapseNavbar();

    $('.carousel').carousel('pause');

    // if (!($('#patrocinadores div').hasClass("patrocinadores-col")) && !($('.sobre-tab li').hasClass("active"))){
    //     $('#patrocinadores').css('background-color','transparent');
    //     $('#patrocinadores').css('border','0px');
    // }else {
    //     $('#patrocinadores').css('background-color','#FFFFFF');
    //     $('#patrocinadores').css('border-bottom','1px solid #e5e5e5');
    // }

    if (isMobile){
        grayzify();
        $('.navbar-custom .navbar-collapse-mobile .panel-group').css('height',getDeviceHeight());
        // $('.intro .container').css('height',getDeviceHeight());
        // putInMiddle('.navbar-header .row', '.social-icons');
        $('section').css("min-height",getDeviceHeight());
        $('#secao-inicial').css("min-height",getDeviceHeight());

        var somaAlturas = $('#patrocinadores').height() + $('.navbar-custom').height();
        $('#esf-background').css("height", getDeviceHeight() - somaAlturas);
    }

    if (screen.width > 768 && window.location.href === "http://localhost:8888/Development/ESF/esf-grayscale/index.html"){ 

        // $('#esf-background').css('background-image','none');
        // $('#esf-background').backstretch([  "img/ESF-header.jpg","img/maria-lages.jpg","img/ESF-header.jpg","img/representante-esf-al.jpg"],{duration: 5000, fade: 1000, centeredY:false});

    }

    changeAllArrows();

    putInMiddle("#contato-section", "#contato-group");
    putInMiddle("#missao-section", "#missao-section .container-fluid");
    putInMiddle(".depoimentos-section", ".depoimentos-section .container-fluid");
    putInMiddle("#midia-section", "#midia-section .container");
    putInMiddle("#acoes-section", " #acoes-section .container");
    putInMiddle("#linha-do-tempo-desktop", "#linha-do-tempo-desktop .container-fluid");
    putInMiddle(".apoie-section", ".apoie-section .container-fluid");

});

// $('.desktop-menu li a').click(function () {
//     // console.log("Item clicked");
//     // console.log(this);

//     if ($(this).hasClass("sobre-id")) {
//         // check if it' active or not
//         console.log("Sobre clicado");
//         if ($('.sobre-tab').css('display') === 'none'){
//             $('.row#patrocinadores').css('background-color','#FFFFFF');
//             $('.sobre-tab').css('display','block');
//             $(this).parent().addClass('active');
//             $(this).parent().siblings().removeClass('active');
//             $('.patrocinadores-col').css('display','none')
//             $('#patrocinadores').css('border-bottom','1px solid #e5e5e5');
//         }
//         else {
//             console.log("Consegui")
//             $('.sobre-tab').css('display','none');
//             $(this).parent().removeClass('active');
//             $('.patrocinadores-col').css('display','block');
//             if (!($('#patrocinadores div').hasClass("patrocinadores-col"))){
//                 $('#patrocinadores').css('background-color','transparent');
//                 $('#patrocinadores').css('border','0px');
//             }
//         }
//     }

// });

function changeArrowUpDown (){
    var that = $(this).find('.fa'); 

    if ($(that).hasClass('fa-caret-down')){
        $(that).removeClass('fa-caret-down')
        .addClass('fa-caret-up');
    }
    else if ($(that).hasClass('fa-caret-up')){
        $(that).removeClass('fa-caret-up')
        .addClass('fa-caret-down');
    } 
}

function changeArrowRightDown(){
    var that = $(this).find('.fa'); 

    if ($(that).hasClass('fa-caret-right')){
        $(that).removeClass('fa-caret-right')
        .addClass('fa-caret-down');
    }
    else if ($(that).hasClass('fa-caret-down')){
        $(that).removeClass('fa-caret-down')
        .addClass('fa-caret-right');
    } 
}

function changeAllArrows () {

    $("#financiador-heading").find('.accordion-toggle').on("click",changeArrowUpDown);
    $("#apoiador-heading").find('.accordion-toggle').on("click",changeArrowUpDown);
    $("#colaborador-heading").find('.accordion-toggle').on("click",changeArrowUpDown);
    $("#tecnico-heading").find('.accordion-toggle').on("click",changeArrowUpDown);

    $("#sobre-heading.panel-heading").find('.panel-title a').on("click",changeArrowRightDown);
    $("#heading-2016.panel-heading").find('.panel-title a').on("click",changeArrowUpDown);
    $("#heading-2015.panel-heading").find('.panel-title a').on("click",changeArrowUpDown);
    $("#heading-2014.panel-heading").find('.panel-title a').on("click",changeArrowUpDown);
    $("#heading-2013.panel-heading").find('.panel-title a').on("click",changeArrowUpDown);

}

function grayzify() {
    $("#financiador-heading").find('.accordion-toggle').on("click",toggleActive);
    $("#apoiador-heading").find('.accordion-toggle').on("click",toggleActive);
    $("#colaborador-heading").find('.accordion-toggle').on("click",toggleActive);
    $("#tecnico-heading").find('.accordion-toggle').on("click",toggleActive);
}
function toggleActive(){
    $(this).parent().addClass("active");
    $(this).click(function () {
       if ($(this).parent().hasClass("active")){
           $(this).parent().removeClass("active");
           // console.log("Have class");
       }else {
           $(this).parent().addClass("active");
           // console.log("Don' Have class");
       }
    });
}

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top()
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
  if ($(this).attr('class') != 'dropdown-toggle active' && $(this).attr('class') != 'dropdown-toggle') {
    $('.navbar-toggle:visible').click();
  }
});

// Function get section's height 
function getDeviceHeight(){
    var deviceHeight = $(window).height();
    return deviceHeight;
}

function getHeight (object) {
    var element = object;
    var height = $(element).height();
    return height;
}

function openNav() {
    document.getElementById("myNav").style.width = "100%";
    // $("#secao-inicial").css("overflow","hidden");
}

function closeNav() {
    document.getElementById("myNav").style.width = "0%";
    // $("#secao-inicial").css("overflow","scroll");
}

function putInMiddle (parentObject, childObject) {
    var parent          = parentObject;
    var child           = childObject;
    var parentHeight    = getHeight(parent);
    var childHeight    = getHeight(child);

    var marginTop = (parentHeight - childHeight) / 3;
    console.log("Parent: "+parent+ "= "+parentHeight);
    console.log("Child: "+child+"= " +childHeight);
    console.log("Margin-top: "+ marginTop);
    $(child).css('margin-top',marginTop);

}

// Verify if site is being accessed through mobile devices
function isMobile () {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        return true
    }
}
// $('.navbar-header button .fa-bars').click(function(){
//     if (!$('.navbar-collapse-mobile').hasClass('collapse in') ||  $('.navbar-collapse-mobile').hasClass('collapsing')) {
//        $(this).fadeOut();
//     }
    
// });
