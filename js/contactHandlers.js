



$(document).ready(function(){

	putInMiddle('.container', 'body');
});


// centralizar conteúdo das paginas de sucesso e erro de formulario de contato
function putInMiddle(elementChild, elementFather) {
    // altura das colunas
    // subtrair da altura da pagina e / 2, o resultado é o margin top
    var child = $(elementChild);
    var father = $(elementFather);

    var childHeight = child.height();
    var fatherHeight = father.height();

    var marginTop = (fatherHeight - childHeight) / 2;

    child.css("margin-top",marginTop);

}

// Verify if site is being accessed through mobile devices
function isMobile () {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        return true
    }
}
